import de.notepass.ttws.api.java.pojo.timetable.Timetable;
import de.notepass.ttws.core.classes.ClassesCalculator;
import de.notepass.ttws.core.timetable.TimeTableParser;

public class Test {

    public static void main(String[] args) throws Exception {
        TimeTableParser.bootstrap();
        System.out.println();
    }
}
