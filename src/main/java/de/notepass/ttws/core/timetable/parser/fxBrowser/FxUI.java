package de.notepass.ttws.core.timetable.parser.fxBrowser;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * This class contains a JavaFX-application which will make it possible to spwan a browser
 */
public class FxUI extends Application {

    public static void main(String[] args) {

    }

    @Override
    public void start(Stage primaryStage) throws Exception {

    }
}
