package de.notepass.ttws.core.timetable.parser.fxBrowser;

import com.google.gson.Gson;
import de.notepass.ttws.api.java.ErrorCode;
import de.notepass.ttws.core.error.TtwsException;
import de.notepass.ttws.core.timetable.parser.fxBrowser.browserAddons.pojo.BrowserBridgeObject;
import de.notepass.ttws.core.timetable.parser.fxBrowser.browserAddons.callback.impl.BrowserDataCollectionCallbackWrapper;
import de.notepass.ttws.core.timetable.parser.fxBrowser.browserAddons.callback.BrowserFinishedCallback;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import netscape.javascript.JSObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * This class spawn a browser-process for a request and acquires the raw data from it
 */
public class TimetableBrowserDataSource {
    public static synchronized BrowserBridgeObject[] getRawData(String clazz, int block) throws Exception {
        final BrowserBridgeObject[][] result = {null};
        final String url = String.format("http://www.asw-berufsakademie.de/fileadmin/download/download/Sked%%20Stundenplan/Studium/%s-%d.%%20Block.html", clazz, block);
        final CountDownLatch lock = new CountDownLatch(1);
        final Exception[] processingException = {null};
        BrowserFinishedCallback callback = new BrowserFinishedCallback() {
            public void onFinished(String json) {
                try {
                    Gson gson = new Gson();
                    result[0] = gson.fromJson(json, BrowserBridgeObject[].class);
                    lock.countDown();
                } catch (Exception e) {
                    onError(e);
                }
            }

            public void onError(Exception e) {
                processingException[0] = e;
                try {
                    lock.countDown();
                } catch (Exception ex) {
                    //Ignore
                }
            }
        };

        final BrowserFinishedCallback callbackWrapper = new BrowserDataCollectionCallbackWrapper(callback);

        Platform.runLater(new Runnable() {
            public void run() {
                //TODO: Only use one webview
                WebView wv = new WebView();
                final WebEngine engine = wv.getEngine();
                //engine.load("file:///C:/Users/kim/Desktop/timetableParser_/example.html");
                System.out.println(new SimpleDateFormat("[dd-MM-yyyy-HH-mm] ").format(new Date(System.currentTimeMillis())) + "Serving request for " + url);
                engine.load(url);
                //engine.load("file:///home/kim/WIA18-2. Block.html");

                engine.getLoadWorker().stateProperty().addListener(
                        new ChangeListener<Worker.State>() {
                            public void changed(ObservableValue<? extends Worker.State> ov, Worker.State oldState, Worker.State newState) {
                                if (newState == Worker.State.SUCCEEDED) {
                                    JSObject win = (JSObject) engine.executeScript("window");
                                    win.setMember("jBridge", callbackWrapper);
                                    engine.executeScript("tables = document.evaluate(\"//table[@style='margin-top:6pt']\", document);\n" +
                                            "currentTable = tables.iterateNext();\n" +
                                            "tablePositionObjects = [];\n" +
                                            "\n" +
                                            "\n" +
                                            "while (currentTable !== null) {\n" +
                                            "    dates = currentTable.getElementsByClassName('t');\n" +
                                            "    tablePositionObject = {};\n" +
                                            "    tablePositionObject.dates = [];\n" +
                                            "    tablePositionObject.hours = [];\n" +
                                            "\n" +
                                            "    for (datesPos=0;datesPos<dates.length;datesPos++) {\n" +
                                            "      tmp = dates[datesPos];\n" +
                                            "      var rect = tmp.getBoundingClientRect();\n" +
                                            "      tablePositionObject.dates.push([rect.top, rect.right, rect.bottom, rect.left, tmp.innerHTML]);\n" +
                                            "    }\n" +
                                            "\n" +
                                            "    currentHours = currentTable.getElementsByClassName('v');\n" +
                                            "    for (currentHour=0;currentHour<currentHours.length;currentHour++) {\n" +
                                            "      tmp = currentHours[currentHour];\n" +
                                            "      var rect = tmp.getBoundingClientRect();\n" +
                                            "      tablePositionObject.hours.push([rect.top, rect.right, rect.bottom, rect.left, tmp.innerHTML]);\n" +
                                            "    }\n" +
                                            "\n" +
                                            "    tablePositionObjects.push(tablePositionObject);\n" +
                                            "\n" +
                                            "    currentTable = tables.iterateNext();\n" +
                                            "}\n" +
                                            "\n" +
                                            "jBridge.onFinished(JSON.stringify(tablePositionObjects));\n");
                                    engine.getLoadWorker().stateProperty().removeListener(this);
                                } else if (newState == Worker.State.CANCELLED) {
                                    callbackWrapper.onError(new TtwsException(ErrorCode.EC400, "Could not fetch data from ASW website"));
                                }
                            }
                        });
            }
        });

        try {
            lock.await(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            processingException[0] = new TtwsException(ErrorCode.EM001, "Timeout while accessing ASW website data");
        }

        if (processingException[0] != null) {
            throw processingException[0];
        }

        return result[0];
    }
}
