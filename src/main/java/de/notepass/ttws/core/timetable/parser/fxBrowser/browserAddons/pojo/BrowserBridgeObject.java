package de.notepass.ttws.core.timetable.parser.fxBrowser.browserAddons.pojo;

/**
 * This object contains the raw data read from the javascript-snipped inside the browser
 */
public class BrowserBridgeObject {
    private String[][] dates;
    private String[][] hours;

    /**
     * Returns all raw-data date-objects which were read (X/Y-Pos and content)-Array
     * @return
     */
    public String[][] getDates() {
        return dates;
    }

    public void setDates(String[][] dates) {
        this.dates = dates;
    }

    /**
     * Returns all raw-data hour-objects which were read (X/Y-Pos and content)-Array
     * @return
     */
    public String[][] getHours() {
        return hours;
    }

    public void setHours(String[][] hours) {
        this.hours = hours;
    }
}
