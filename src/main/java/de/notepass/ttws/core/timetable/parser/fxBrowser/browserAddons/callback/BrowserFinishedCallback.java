package de.notepass.ttws.core.timetable.parser.fxBrowser.browserAddons.callback;

/**
 * This is an interface for creating a callback for the javascript -> java interface
 */
public interface BrowserFinishedCallback {
    public void onFinished(String json);
    public void onError(Exception e);
}
