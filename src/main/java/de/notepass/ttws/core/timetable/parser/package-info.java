/**
 * This package contains the "magic" used for creating Timetable-objects from the raw data read by the fxBrowser
 */
package de.notepass.ttws.core.timetable.parser;