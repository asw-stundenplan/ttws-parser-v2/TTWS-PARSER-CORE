package de.notepass.ttws.core.timetable.parser;

import de.notepass.ttws.api.java.pojo.timetable.Timetable;
import de.notepass.ttws.api.java.pojo.timetable.TimetableDay;
import de.notepass.ttws.api.java.pojo.timetable.TimetableHour;
import de.notepass.ttws.api.java.pojo.timetable.TimetableWeek;
import de.notepass.ttws.core.timetable.parser.fxBrowser.browserAddons.pojo.BrowserBridgeObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Pattern;

public class TimetableParserUtils {
    public static SimpleDateFormat dateOnlyFormatter = new SimpleDateFormat("E, dd.MM.yyyy", Locale.GERMANY);
    public static SimpleDateFormat timeOnlyFormatter = new SimpleDateFormat("HH:mm", Locale.GERMANY);

    /**
     * This method creates a TimetableDay-Object from the raw data read by the browser
     * @param date This array contains the X and Y positions and content of the day inside the Timetable website
     * @param hours This array contains all found hours (X-Pos, Y-Pos, Content), which is assigned to the given
     *              day by position. Hours which do not belong to the given day are ignored<br/>
     *              After the hour is assigned to the given day the content is parsed.
     * @return Requested day as TimetableDay-object
     */
    public static TimetableDay dayFromRawData(String[] date, String[][] hours) throws ParseException {
        TimetableDay day = new TimetableDay();
        int dateLeftBound = Float.valueOf(date[TimetableParserConstants.X]).intValue();
        int dateRightBound = Float.valueOf(date[TimetableParserConstants.Y]).intValue();
        day.setDate(
                dateOnlyFormatter.parse(date[TimetableParserConstants.DATA_POS_INDEX])
        );

        for (String[] hour : hours) {
            int hourLeftBound = Float.valueOf(hour[TimetableParserConstants.X]).intValue();
            int hourRightBound = Float.valueOf(hour[TimetableParserConstants.Y]).intValue();

            if (isBetween(dateLeftBound, dateRightBound, hourLeftBound, hourRightBound)) {
                String hourData = hour[TimetableParserConstants.DATA_POS_INDEX]; //ex.: 13:00 - 14:30 UhrVorlesungWIMATNK: 1.07
                String[] data = hourData.split(Pattern.quote("<br>"));
                String[] times = data[0].trim().split(Pattern.quote("-"));
                String startTime = times[0].trim();
                String endTime = times[1].trim();
                endTime = endTime.substring(0, endTime.indexOf("Uhr")).trim();
                String type = data[1].trim();
                String subject = data[2].trim();
                String room = "";
                String location = "";
                if (data.length > 3) {
                    room = data[3].trim();
                    String[] data2 = room.split(":");
                    if (data2.length > 1) {
                        room = data2[1].trim();
                        location = data2[0].trim();
                    }
                }
                TimetableHour tthour = new TimetableHour();
                tthour.setStartTime(timeOnlyFormatter.parse(startTime));
                tthour.setEndTime(timeOnlyFormatter.parse(endTime));
                tthour.setSubject(subject);
                tthour.setRoom(room);
                tthour.setLocation(location);
                tthour.setType(type);
                day.getHours().add(tthour);
            }
        }

        return day;
    }

    /**
     * This function creates a Timetable-Object from the given raw-data array
     * @param data
     * @return
     * @throws ParseException
     */
    public static Timetable timetableFromRawData(BrowserBridgeObject[] data) throws ParseException {
        Timetable timetable = new Timetable();

        for (BrowserBridgeObject o:data) {
            TimetableWeek week = new TimetableWeek();

            for (String[] cdate:o.getDates()) {
                TimetableDay day = dayFromRawData(cdate, o.getHours());
                week.getDays().add(day);
            }

            timetable.getWeeks().add(week);
        }

        return postfixContentErrors(timetable);
    }

    /**
     * Example of element to check
     * <pre>
     *     yo-------------xo
     *      yi-----------xi
     * </pre>
     * @param xo x-cord of outer element
     * @param yo y-cord of outer element
     * @param xi x-cord of inner element
     * @param yi y-cord of inner element
     * @return If xi/yi are inside the bounds of xo/yo
     */
    public static boolean isBetween(int xo, int yo, int xi, int yi) {
        return xo <= xi && yo >= yi;
    }

    /**
     * Example of element to check
     * <pre>
     *     yo-------------xo
     *      yi-----------xi
     * </pre>
     * @param xo x-cord of outer element
     * @param yo y-cord of outer element
     * @param xi x-cord of inner element
     * @param yi y-cord of inner element
     * @param padding additional padding for xo(yo
     * @return If xi+padding/yi+padding are inside the bounds of xo/yo
     */
    public static boolean isBetween(int xo, int yo, int xi, int yi, int padding) {
        return isBetween(xo+padding, yo+padding, xi, yi);
    }

    /**
     * This function fixed some of the "errors" which appear while parsing the data
     * @param t
     * @return
     */
    public static Timetable postfixContentErrors(Timetable t) {
        //Step 1: Remove empty weeks/days
        for (int i=0;i<t.getWeeks().size();i++) {
            TimetableWeek week = t.getWeeks().get(i);
            for (int j=0;j<week.getDays().size();j++) {
                TimetableDay day = week.getDays().get(j);
                if (day.getHours().size() <= 0) {
                    week.getDays().remove(j);
                    j--; //Keep index to not jump an entry
                }
            }

            if (week.getDays().size() <= 0) {
                t.getWeeks().remove(i);
                i--; //Keep index to not jump an entry
            }
        }

        //Step 2: Calculate start and end dates of the timetable/weeks
        Date earliestWeekDate = null;
        Date latestWeekDate = null;

        for (TimetableWeek week:t.getWeeks()) {
            Date earliestDayDate = null;
            Date latestDayDate = null;

            for (TimetableDay day:week.getDays()) {
                if (earliestDayDate == null) {
                    earliestDayDate = day.getDate();
                } else if (earliestDayDate.compareTo(day.getDate()) > 0) {
                    earliestDayDate = day.getDate();
                }

                if (latestDayDate == null) {
                    latestDayDate = day.getDate();
                } else if (latestDayDate.compareTo(day.getDate()) < 0) {
                    latestDayDate = day.getDate();
                }
            }

            week.setStartDate(earliestDayDate);
            week.setEndDate(latestDayDate);

            if (earliestWeekDate == null) {
                earliestWeekDate = week.getStartDate();
            } else if (earliestWeekDate.compareTo(week.getStartDate()) > 0) {
                earliestWeekDate = week.getStartDate();
            }

            if (latestWeekDate == null) {
                latestWeekDate = week.getEndDate();
            } else if (latestWeekDate.compareTo(week.getEndDate()) < 0) {
                latestWeekDate = week.getEndDate();
            }
        }

        t.setStartDate(earliestWeekDate);
        t.setEndDate(latestWeekDate);

        return t;
    }
}
