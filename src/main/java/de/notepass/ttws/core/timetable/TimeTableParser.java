package de.notepass.ttws.core.timetable;

import de.notepass.ttws.api.java.pojo.timetable.Timetable;
import de.notepass.ttws.api.java.pojo.timetable.TimetableWeek;
import de.notepass.ttws.core.timetable.parser.TimetableParserUtils;
import de.notepass.ttws.core.timetable.parser.fxBrowser.browserAddons.pojo.BrowserBridgeObject;
import de.notepass.ttws.core.timetable.parser.fxBrowser.FxUI;
import de.notepass.ttws.core.timetable.parser.fxBrowser.TimetableBrowserDataSource;
import javafx.application.Application;

import java.security.InvalidParameterException;

/**
 * This class contains methods to access the timetable-parser
 */
public class TimeTableParser {

    //TODO: Calculate start and end dates

    protected static Thread fxApplicationThread = null;

    /**
     * Returns the timetable for a single block
     *
     * @param clazz Class to request the timetable for (e.g. WIA16)
     * @param block Block to request timetable for (e.g. 8)
     * @return Timetable-object with all hours for the given block and class or null if no data was found
     */
    public static Timetable getTimetableForBlock(String clazz, int block) throws Exception {
        bootstrap();
        BrowserBridgeObject[] rawData = TimetableBrowserDataSource.getRawData(clazz, block);
        return TimetableParserUtils.timetableFromRawData(rawData);
    }

    /**
     * Returns the timetable for multiple blocks
     *
     * @param clazz      Class to request the timetable for (e.g. WIA16)
     * @param startBlock Start of the block range
     * @param endBlock   End of the block range
     * @return Timetable-object with all hours for the given blocks and class or null if no data was found
     */
    public static Timetable getTimetableForBlockRange(String clazz, int startBlock, int endBlock) throws Exception {
        if (endBlock < startBlock) {
            throw new InvalidParameterException("endBlock should not be smaller than startBlock");
        }

        if (endBlock < 0) {
            throw new InvalidParameterException("endBlock should not be smaller than 1");
        }

        if (startBlock < 0) {
            throw new InvalidParameterException("startBlock should not be smaller than 1");
        }

        bootstrap();

        Timetable container = new Timetable();
        for (int i = startBlock; i <= endBlock; i++) {
            Timetable sub = getTimetableForBlock(clazz, i);
            for (TimetableWeek week : sub.getWeeks()) {
                container.getWeeks().add(week);
            }
        }

        return container;
    }

    public static synchronized void bootstrap() {
        if (fxApplicationThread == null) {
            System.out.println("bootstrapping");

            fxApplicationThread = new Thread(new Runnable() {
                public void run() {
                    Application.launch(FxUI.class);
                }
            });

            fxApplicationThread.start();
        }
    }
}