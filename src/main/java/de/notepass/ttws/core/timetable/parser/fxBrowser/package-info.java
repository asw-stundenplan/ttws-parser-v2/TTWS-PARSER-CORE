/**
 * This package contains the java-browser bridge, which requests the timetable and calculates the position of the
 * different Elements. With this, the calculation of the hours is done.
 **/
package de.notepass.ttws.core.timetable.parser.fxBrowser;