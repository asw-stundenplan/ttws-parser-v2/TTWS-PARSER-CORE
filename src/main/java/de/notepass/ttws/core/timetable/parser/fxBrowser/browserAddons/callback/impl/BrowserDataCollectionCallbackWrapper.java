package de.notepass.ttws.core.timetable.parser.fxBrowser.browserAddons.callback.impl;

import de.notepass.ttws.core.timetable.parser.fxBrowser.browserAddons.callback.BrowserFinishedCallback;

/**
 * This is a wrapper class for the callback defined inside the "TimetableBrowserDataSource", as the javascript bridge
 * can't interact with anonymous interface implementations
 */
public class BrowserDataCollectionCallbackWrapper implements BrowserFinishedCallback {
    private BrowserFinishedCallback realCallback;

    public BrowserDataCollectionCallbackWrapper(BrowserFinishedCallback realCallback) {
        this.realCallback = realCallback;
    }

    public void onFinished(String json) {
        realCallback.onFinished(json);
    }

    public void onError(Exception e) {
        realCallback.onError(e);
    }
}
