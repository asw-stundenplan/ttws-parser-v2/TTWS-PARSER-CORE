package de.notepass.ttws.core.error;

import de.notepass.ttws.api.java.ErrorCode;

import java.util.*;

public class TtwsException extends Exception {
    private ErrorCode errorCode;
    private Map<String, String> additionalData = new HashMap<>();

    public static final String ADDITIONAL_DATA_KEY_MESSGAE_PLACEHOLDER_1 = "eMSGplh1";
    public static final String ADDITIONAL_DATA_KEY_MESSGAE_PLACEHOLDER_2 = "eMSGplh2";
    public static final String ADDITIONAL_DATA_KEY_MESSGAE_PLACEHOLDER_3 = "eMSGplh3";
    public static final String ADDITIONAL_DATA_KEY_MESSGAE_PLACEHOLDER_4 = "eMSGplh4";
    public static final String ADDITIONAL_DATA_KEY_MESSGAE_PLACEHOLDER_5 = "eMSGplh5";

    public TtwsException(ErrorCode errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    public TtwsException(ErrorCode errorCode, String message, Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public TtwsException(ErrorCode errorCode, String message, Map<String, String> additionalData) {
        super(message);
        this.errorCode = errorCode;
        this.additionalData = additionalData;
    }

    public TtwsException(ErrorCode errorCode, String message, Map<String, String> additionalData, Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
        this.additionalData = additionalData;
    }

    public String getAdditionalData(String key) {
        return additionalData.get(key);
    }

    public void addAdditionalData(String key, String value) {
        additionalData.put(key, value);
    }

    public String[] getErrorMessagePlaceholders() {
        if (getErrorCode() != ErrorCode.EM001) {
            List<String> ph = new ArrayList<>(3);
            ph.add(additionalData.get(ADDITIONAL_DATA_KEY_MESSGAE_PLACEHOLDER_1));
            ph.add(additionalData.get(ADDITIONAL_DATA_KEY_MESSGAE_PLACEHOLDER_2));
            ph.add(additionalData.get(ADDITIONAL_DATA_KEY_MESSGAE_PLACEHOLDER_3));
            ph.add(additionalData.get(ADDITIONAL_DATA_KEY_MESSGAE_PLACEHOLDER_4));
            ph.add(additionalData.get(ADDITIONAL_DATA_KEY_MESSGAE_PLACEHOLDER_5));

            for (int i = ph.size() - 1; i >= 0; i--) {
                if (ph.get(i) == null) {
                    ph.remove(i);
                } else {
                    break;
                }
            }

            return ph.toArray(new String[ph.size()]);
        } else {
            if (getCause() != null) {
                return new String[]{getCause().getClass().getCanonicalName()+": "+getCause().getMessage()};
            } else {
                return new String[]{""};
            }
        }
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }
}
