package de.notepass.ttws.core.classes;

import de.notepass.ttws.api.java.pojo.classes.AswClass;
import de.notepass.ttws.api.java.pojo.classes.AswClassCollection;
import de.notepass.ttws.api.java.pojo.classes.AswClassShortName;
import de.notepass.ttws.api.java.pojo.classes.ClassType;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

/**
 * This class calculates which classes are currently existing
 */
public class ClassesCalculator {
    private final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("ddMMyyyy");
    private final static int MONTH_AUGUST = 8;

    /* PSA: Following you will find a short description what each array-index means because its becoming confusing AF:
    0 - shortId: For creating the asw-website access-url
    1 - Long name, as a readable label
    2 - shortName - Value out of a set of defined values, could be handy for people who will create stuff with the according API-endpoint
    3 - Blocks for junior year, separated by "|" (This is the moment I realized that a two-dimensional-string-array is shit for this type of configuration
    4 - Blocks for "between" year, separated by "|" (Realizing it's even shittier)
    5 - Blocks for senior year, separated by "|" (TODO: Make a config file instead of this stuff)
    */

    private static final String[][] EXISTING_CLASSES_WINFO = new String[][]{
            new String[]{"WIA%02d", "Wirtschaftsinformatik, Klasse A, Jahrgang %d", AswClassShortName.WIA.name(), "1|2|3", "4|5|6", "7|8|9"},
            new String[]{"WIB%02d", "Wirtschaftsinformatik, Klasse B, Jahrgang %d", AswClassShortName.WIB.name(), "1|2|3", "4|5|6", "7|8|9"},
            new String[]{"WIC%02d", "Wirtschaftsinformatik, Klasse C, Jahrgang %d", AswClassShortName.WIC.name(), "1|2|3", "4|5|6", "7|8|9"},
            new String[]{"WID%02d", "Wirtschaftsinformatik, Klasse D, Jahrgang %d", AswClassShortName.WID.name(), "1|2|3", "4|5|6", "7|8|9"}
    };

    private static final String[][] EXISTING_CLASSES_WIRT = new String[][]{
            new String[]{"A%02d", "Betriebswirtschaft, Vertiefungsrichtung Industrie, Jahrgang %d", AswClassShortName.A.name(), "1|2|3", "4|5|6", "7|8|9"},
            new String[]{"B%02d", "Betriebswirtschaft, Vertiefungsrichtung Handel, Jahrgang %d", AswClassShortName.B.name(), "1|2|3", "4|5|6", "7|8|9"},
            new String[]{"D%02d", "Betriebswirtschaft, Vertiefungsrichtung Finanzdienstleistungen, Jahrgang %d", AswClassShortName.D.name(), "1|2|3", "4|5|6", "7|8|9"},
            new String[]{"E%02d", "Betriebswirtschaft, Vertiefungsrichtung Taxation & Accounting, Jahrgang %d", AswClassShortName.E.name(), "1|2|3", "4|5|6", "7|8|9"},
            new String[]{"F%02d", "Betriebswirtschaft, Vertiefungsrichtung Logistik, Jahrgang %d", AswClassShortName.F.name(), "1|2|3", "4|5|6", "7|8|9"}
    };

    private static final String[][] EXISTING_CLASSES_MB = new String[][]{
            new String[]{"MB%02d", "Studiengang Maschienenbau, Jahrgang %d", AswClassShortName.MB.name(), "1|2", "3|4", "5|6"}
    };

    private static final String[][] EXISTING_CLASSES_WING = new String[][]{
            new String[]{"WING%02d", "Studiengang Wirtschaftsingenieurwesen, Jahrgang %d", AswClassShortName.WING.name(), "1|2", "3|4", "5|6"}
    };

    public static AswClassCollection getExistingClasses() {
        AswClassCollection classCollection = new AswClassCollection();

        //There are currently 11 classes active at any given time (theoretically):
        //5 Wirtschaftsklassen
        //4 WINFO
        //1 Maschienenbau
        //1 Wirtschaftsingenieurwesen
        List<AswClass> classes = new ArrayList<>(12);

        //Step 1: Get current month and year (b/c classes change every AUGUST [8])
        Date currentDate = new Date(); //Yes, yes, this is terribly outdated, yada, yada
        int year = currentDate.getYear() + 1900;
        int month = currentDate.getMonth() + 1;


        //Step 2: Calculate classes
        //Step 2.1: WINFO
        for (String[] contextClass : EXISTING_CLASSES_WINFO) {
            int currentClassYearSenior = 0;
            int currentClassYearBetween = 0;
            int currentClassYearJunior = 0;
            if (month < MONTH_AUGUST) {
                currentClassYearSenior = year - 2000;
                currentClassYearBetween = year - 1999;
                currentClassYearJunior = year - 1998;
            } else {
                currentClassYearSenior = year - 1999;
                currentClassYearBetween = year - 1998;
                currentClassYearJunior = year - 1997;
            }

            writeEntries(classes, contextClass, currentClassYearSenior, currentClassYearBetween, currentClassYearJunior);
        }

        //Step 2.2: WIRT
        for (String[] contextClass : EXISTING_CLASSES_WIRT) {
            int currentClassYearSenior = 0;
            int currentClassYearBetween = 0;
            int currentClassYearJunior = 0;
            if (month < MONTH_AUGUST) {
                currentClassYearSenior = year - 1993;
                currentClassYearBetween = year - 1992;
                currentClassYearJunior = year - 1991;
            } else {
                currentClassYearSenior = year - 1992;
                currentClassYearBetween = year - 1991;
                currentClassYearJunior = year - 1990;
            }

            writeEntries(classes, contextClass, currentClassYearSenior, currentClassYearBetween, currentClassYearJunior);
        }

        //Step 2.3: MB
        for (String[] contextClass : EXISTING_CLASSES_MB) {
            int currentClassYearSenior = 0;
            int currentClassYearBetween = 0;
            int currentClassYearJunior = 0;
            if (month < MONTH_AUGUST) {
                currentClassYearSenior = year - 2004;
                currentClassYearBetween = year - 2003;
                currentClassYearJunior = year - 2002;
            } else {
                currentClassYearSenior = year - 2003;
                currentClassYearBetween = year - 2002;
                currentClassYearJunior = year - 2001;
            }

            writeEntries(classes, contextClass, currentClassYearSenior, currentClassYearBetween, currentClassYearJunior);
        }

        //Step 2.4: WING
        for (String[] contextClass : EXISTING_CLASSES_WING) {
            int currentClassYearSenior = 0;
            int currentClassYearBetween = 0;
            int currentClassYearJunior = 0;
            if (month < MONTH_AUGUST) {
                currentClassYearSenior = year - 2009;
                currentClassYearBetween = year - 2008;
                currentClassYearJunior = year - 2007;
            } else {
                currentClassYearSenior = year - 2008;
                currentClassYearBetween = year - 2007;
                currentClassYearJunior = year - 2006;
            }

            writeEntries(classes, contextClass, currentClassYearSenior, currentClassYearBetween, currentClassYearJunior);
        }

        try {
            if (month < MONTH_AUGUST) {
                classCollection.setValidFrom(DATE_FORMAT.parse("0108" + (year - 1)));
                classCollection.setValidTill(DATE_FORMAT.parse("3107" + year));
            } else {
                classCollection.setValidFrom(DATE_FORMAT.parse("0108" + year));
                classCollection.setValidTill(DATE_FORMAT.parse("3107" + (year + 1)));
            }
        } catch (Exception e) {
            //Shouldn't happen, but still:
            //TODO: Logging
            e.printStackTrace();
        }

        classCollection.setClasses(classes);

        return classCollection;
    }

    private static void writeEntries(List<AswClass> classes,
                                     String[] contextClass,
                                     int currentClassYearSenior,
                                     int currentClassYearBetween,
                                     int currentClassYearJunior) {
        String[] blocksJuniorRaw, blocksBetweenRaw, blocksSeniorRaw;
        int[] blocksJunior, blocksBetween, blocksSenior;
        blocksJuniorRaw = contextClass[3].split(Pattern.quote("|"));
        blocksBetweenRaw = contextClass[4].split(Pattern.quote("|"));
        blocksSeniorRaw = contextClass[5].split(Pattern.quote("|"));

        blocksJunior = new int[blocksJuniorRaw.length];
        blocksBetween = new int[blocksBetweenRaw.length];
        blocksSenior = new int[blocksSeniorRaw.length];

        //This is completely stupid
        for (int i = 0; i < blocksJuniorRaw.length; i++) {
            blocksJunior[i] = Integer.parseInt(blocksJuniorRaw[i]);
        }

        for (int i = 0; i < blocksBetweenRaw.length; i++) {
            blocksBetween[i] = Integer.parseInt(blocksBetweenRaw[i]);
        }

        for (int i = 0; i < blocksSeniorRaw.length; i++) {
            blocksSenior[i] = Integer.parseInt(blocksSeniorRaw[i]);
        }

        classes.add(new AswClass(
                        String.format(contextClass[0], currentClassYearSenior),
                        String.format(contextClass[1], currentClassYearSenior),
                        AswClassShortName.valueOf(contextClass[2]),
                        currentClassYearSenior,
                        ClassType.SENIOR,
                        blocksSenior
                )
        );

        classes.add(new AswClass(
                        String.format(contextClass[0], currentClassYearBetween),
                        String.format(contextClass[1], currentClassYearBetween),
                        AswClassShortName.valueOf(contextClass[2]),
                        currentClassYearBetween,
                        ClassType.BETWEEN,
                        blocksBetween
                )
        );

        classes.add(new AswClass(
                        String.format(contextClass[0], currentClassYearJunior),
                        String.format(contextClass[1], currentClassYearJunior),
                        AswClassShortName.valueOf(contextClass[2]),
                        currentClassYearJunior,
                        ClassType.JUNIOR,
                        blocksJunior
                )
        );
    }
}
